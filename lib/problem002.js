
var fib = [1,2];
var i = 1;
var sum = 0;

while (fib[i] < 4000000) {

  fib.push(fib[i] + fib[i-1]);
  if ((fib[i] < 4000000) && ((fib[i] % 2) === 0)) sum += fib[i];
  i++;

}

console.log(sum);