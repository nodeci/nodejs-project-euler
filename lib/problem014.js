/*
  The following iterative sequence is defined for the set of positive integers:

  n -> n/2 (n is even)
  n -> 3n + 1 (n is odd)

  Using the rule above and starting with 13, we generate the following sequence:

  13  -> 40  -> 20  -> 10  -> 5  -> 16  -> 8  -> 4  -> 2  -> 1
  It can be seen that this sequence (starting at 13 and finishing at 1)
  contains 10 terms. Although it has not been proved yet (Collatz Problem),
  it is thought that all starting numbers finish at 1.

  Which starting number, under one million, produces the longest chain?

  NOTE: Once the chain starts the terms are allowed to go above one million.

*/

// Starting Point [main()?]
getLongestChain(1000000);

function getLongestChain (highest) {
  var maxNumber = 0;
  var maxLength = 0;
  for (var i = highest; i > 0; i--) {
    var l = getSequence(i).length;
    if (l > maxLength) {
      maxLength = l;
      maxNumber = i;
    }
  }

  return console.log('Longest Chain Belongs to ' + maxNumber);
}


function getSequence (number) {
  var result = [];
  result[0] = number;
  var i = 0;

  while(result[i] != 1) {
    if (result[i] % 2 === 0) {
      result.push(result[i] / 2);
    } else {
      result.push((3 * result[i]) + 1);
    }
    i++;
  }

  return result;
}

