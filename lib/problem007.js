
//
var start = Date.now();

// INIT
var limit = 130000;
var n = {};

for (i = 2; i <= limit; i++) {
  n[i] = true;
}

// Sieve
for (i = 2; i <= limit; i++) {
  for (j = 2; j < i; j++) {
    if (n[i] === true) {
      if ((i % j) === 0) n[i] = false;
    }
  };
};

// OUTPUT
var index = 1;

for (i = 2; i <= limit; i++) {
  if(n[i] === true) {
    console.log(index + ' : ' + i);
    if (index === 10001) break;
    index++;
  }
};

var end = Date.now() - start;
console.log('Took ' + end + ' ms.')