var i = 2;

while (true) {
  var flagFoundCondition = true;

  for (var j = 2; j <= 20; j++) {
    if ((i % j) !== 0) flagFoundCondition = false;
  };

  if (flagFoundCondition) break;
  i++;
}

console.log(i);
