
for (var a = 1; a <= 998; a++) {
  for (var b = 1; b <= 998; b++) {
    var equation = a * a + b * b - (1000 - a - b) * (1000 - a - b);
    if (equation === 0) showSolution(a,b);
  }
}

function showSolution (a,b) {
  var sol = a * b * (1000 - a - b);
  console.log(sol);
  process.exit(0);
};
